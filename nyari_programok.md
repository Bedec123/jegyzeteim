# Nyári programok és egyéb jegyzetek a 92. Szent István cs.cs. számára #
---
#### Mivel nem lesz nagytábor és tábort se tudnánk megszervezni ezért programokkal töltsük ki a nyarat ;) :D ####
---

### 1. Sportnap - `Július 26.` ###
###### Cél : beindítani a nyári programokat egy kötetlen sportolással és találkozással ######
**Helyszín** : Reztető / ~~Valahol máshol nagy hely lekaszált hely~~

**Program** :
* **13:15** - gyülekező
* **13:30** - indulás
* **14:00** - érkezés
* **14:10** - zászlólopás
* **14:50** - dark stories
* **15:10** - frizbi
* **15:40** - dark stries
* **16:00** - méta
* **16:50** - közös kaja
* **17:30** - számháború
* **18:30** - indulás haza

**Hoznivalók:**
- étel saját tarisznya
- minimum 2L víz
- **Kalap**
- esőkabát
- valami amire leülhetsz (pokróc, izolír...)
- wc papír
- **cserkész nyakkendő**
- minden szükséges ami egy fél napos portyára kell

**Feladatok:**
* [X] zászlók készítése
* [X] megkülönböztető jelzések készítése
* [ ] számháborús pántok készítése
* [X] játék és egyéb felszereléseket nem otthon hagyni
    * [ ] alap elsősegély készlet
    * [X] méta ütő és labda
    * [X] frizbi
    * [ ] számháborússzámok
    * [X] zászlók
    * [X] megkülönböztető pántok

**Egyéb:**
Méta szabályzat:
* 8 perc játékidő
* a kiesett játékosok élveznek elsőbséget az ütésnél

**Kiértékelés:**

- [X] Kész

---

### 2. Csillagász - `Augusztus 16.` ###
###### Cél: élményt és egy kevés tudást nyújtani a csillagászattal kapcsolataban a cserkészek számára a szülúknek nyílt program. Próbáljuk bevonzani a Szentkirályi felnőtteket ######
**Helyszín** : Süketné

**Program** : *folyamatban...*

**Feladatok** : 
- [ ] *_Próba anyag közzététele vasárnapig_*
- [ ] Keretmese kidolgozas
- [ ] Program elkészítése
- [ ] Kaja megbeszélés/ leszervezés
- [ ] felszerelés elkérése
- [ ] Távcsovek
- [ ] Bogrács kásának
- [ ] Tűzhely
- [ ] Nagysátrak
- [ ] Meghirdetés
- [ ] Szülőknek szólni
- [ ] Segédeket keresni

- *többi folyamatban...*


- [ ] Kész
---

### 3. Filmest - `Augusztus 22.|23.` ###
###### Cél: Program szervezése úgy a cserkészeknek mint a faluban lévő embereknek. Ezáltal népszerűsíteni a cserkészetet és a cserkészcsapatot ######
**Helyszín** : PapUdvar


**Filmeötletek** :
- Agymanók
- Emlékek őrzői
- Instant Család
- Add tovább
- Bővitendő

**Program** : *folyamatban...*

**Feladatok** : *folyamatban...*



- [ ] Kész
---

### 4. Hargita nagytúra és fogadalomtétel - ( Augusztus 5. nem hiszem, hogy meg tudjuk szervezni )###
###### Cél: cserkészek próbáztatása  ######
**Helyszín** : Hargita ??? ( ezt még át kell gondolnunk hogy legyen )

**Program** : *folyamatban...*

**Feladatok** : *folyamatban...*


- [ ] Kész
---
Nyakkendő - Név lista 
---

## 1. Kék nyakkendő : ##
1. Bálint Barni
2. Fazakas Zalán
3. Bálint Kristóf


## 1. pöttyüs nyakkendő
4. Lőrincz Tündike
5. Bálint Otilia
6. Bálint Angella

**Össz: 5**


- [ ] Kész
---

## 2. Zöld nyakkendő : ##
1. Dakó András
2. László Hanna
3. Péter Alpár
4. Fazakas Andrea
5. Bálint András
6. László Bálint
7. Kovács Dóra
8. Bálint Lukács
9. Balázs Boróka
10. Bálint Lóránd
11. László István
12. Balázs Péter
13. Péter Vanda
14. Bálint Zsanett
15. Telekfalvi Réka
16. Bálint Róbert
17. Simó Attila
18. Balázs Rebeka
**Össz : 18**


- [ ] Kész
---

## 3. Szürke nyakkendő : ##
1. Dakó Boglárka
2. Benedek Barbara
3. Fazakas Tímea
4. Bálint Boglárka
5. László Zsofia
6. Bodó Orsolya
**Össz: 6**


- [ ] Kész
